package com.company;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Main {

    private static final String PATH_TO_FILE = "weather_table.csv";

    public static void main(String[] args) throws Exception {
        WeatherFileManager fileManager = new WeatherFileManager(new File(Paths.get(PATH_TO_FILE).toUri()));
        List<Weather> weatherList = fileManager.readWeatherFromFile();
        WeatherManager weatherManager = new WeatherManagerImpl(weatherList);

        File file = new File("weather_list.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        SimpleDateFormat format = new SimpleDateFormat("dd.MM, HH:mm");

        writer.println("Средняя температура [ " + weatherManager.countAverageTemperature() + " °C ]");
        writer.println("Средняя влажность [ " + weatherManager.countAverageHumidity() + " % ]");
        writer.println("Средняя скорость ветра [ " + weatherManager.countAverageWindSpeed() + " km/h ]");
        writer.println();
        Weather maxTempDto = weatherManager.findMaxTemp();
        Date maxTempDtoDate = maxTempDto.getDate();
        writer.println("Максимальная температура [ " + maxTempDto.getTemperature() + " °C ]" +
                " была зафиксированана " + format.format(maxTempDtoDate));
        Weather minHumidityDto = weatherManager.findMinHumidity();
        Date minHumidityDtoDate = minHumidityDto.getDate();
        writer.println("Минимальная влажность [ " + minHumidityDto.getHumidity() + " % ]" +
                " была зафиксирована " + format.format(minHumidityDtoDate));
        Weather maxWindSpeedDto = weatherManager.findMaxWindSpeed();
        Date maxWindSpeedDtoDate = maxWindSpeedDto.getDate();
        writer.println("Максимальная скорость ветра [ " + maxWindSpeedDto.getWindSpeed() + " km/h ]" +
                " была зафиксирована " + format.format(maxWindSpeedDtoDate));


        writer.println("Самое частое направление ветра [ " + weatherManager.findCommonWindDirection() + " ]");

        writer.close();
    }
}