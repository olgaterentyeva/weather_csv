package com.company;

import java.util.Date;

public class Weather {
    private final Date date;
    private final Double temperature;
    private final Double humidity;
    private final Double windSpeed;
    private final Double windDirection;

    public Weather(Date date, Double temperature, Double humidity, Double windSpeed, Double windDirection) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    public Date getDate() {
        return date;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Double getWindSpeed() { return windSpeed; }

    public Double getWindDirection() { return windDirection;}

    @Override
    public String toString() {
        return "WeatherDto >> \n" +
                "DATA: " + date +
                "; TEMPERATURE: " + temperature +
                "; HUMIDITY: " + humidity +
                "; WIND_SPEED: " + windSpeed +
                "; WIND_DIRECTION" + windDirection;
    }
}

