package com.company;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class WeatherFileManager {

    private final File srcFile;

    public WeatherFileManager(File srcFile) {
        this.srcFile = srcFile;
    }

    public List<Weather> readWeatherFromFile() throws Exception {
        List<Weather> weatherDtos = new ArrayList<>();

        Scanner scanner = new Scanner(srcFile);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String [] parts = line.split(",");
            if (isWeather(parts)) {
                Weather weatherDto = deserializeWeather(parts);
                weatherDtos.add(weatherDto);
            }
        }
        return weatherDtos;
    }

    public boolean isWeather(String[] parts) {
        String firstPart = parts[0];
        char ch = firstPart.charAt(0);
        return ch >= '0' && ch <= '9';
    }

    private Weather deserializeWeather(String [] parts) throws Exception {

        if (parts.length < 5)  return null;
        else {
            String dataStr = parts[0];
            // делаем из стоки дату
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmm");

            // делаем парсинг
            Date trueDate = format.parse(dataStr);
            Double temperature = Double.parseDouble(parts[1]);
            Double humidity = Double.parseDouble(parts[2]);
            Double windSpeed = Double.parseDouble(parts[3]);
            Double windDirection = Double.parseDouble(parts[4]);

            Weather dto = new Weather(trueDate, temperature, humidity, windSpeed, windDirection);

            return dto;
        }
    }
}
