package com.company;

public interface WeatherManager {

    Double countAverageTemperature();
    Double countAverageHumidity();
    Double countAverageWindSpeed();

    Weather findMaxTemp();
    Weather findMinHumidity();
    Weather findMaxWindSpeed();

    String findCommonWindDirection();

}
