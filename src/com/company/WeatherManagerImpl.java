package com.company;

import java.util.*;

public class WeatherManagerImpl implements WeatherManager {

    private final List<Weather> weatherDtos;

    public WeatherManagerImpl(List<Weather> weatherDtos) {
        this.weatherDtos = weatherDtos;
    }

    @Override
    public Double countAverageTemperature() {

        int count = 0;
        double result = 0.0;
        for (Weather dto : weatherDtos) {
            result += dto.getTemperature();
            count++;
        }
        result /= count;
        return result;
    }

    @Override
    public Double countAverageHumidity() {

        int count = 0;
        double result = 0.0;
        for (Weather dto : weatherDtos) {
            result += dto.getHumidity();
            count++;
        }
        result /= count;
        return result;
    }

    @Override
    public Double countAverageWindSpeed() {

        int count = 0;
        double result = 0.0;
        for (Weather dto : weatherDtos) {
            result += dto.getWindSpeed();
            count++;
        }
        result /= count;
        return result;
    }

    @Override
    public Weather findMaxTemp() {

        double maxTemp;
        List<Double> tempList = new ArrayList<>();
        for (Weather weatherList : weatherDtos) {
            tempList.add(weatherList.getTemperature());
        }
        maxTemp = Collections.max(tempList);
        for (Weather weatherList : weatherDtos) {
            if (weatherList.getTemperature() == maxTemp) {
                return weatherList;

            }
        }
        return null;
    }

    @Override
    public Weather findMinHumidity() {

        double minHumidity;
        List<Double> tempList = new ArrayList<>();
        for (Weather weatherList : weatherDtos) {
            tempList.add(weatherList.getHumidity());
        }
        minHumidity = Collections.min(tempList);
        for (Weather weatherList : weatherDtos) {
            if (weatherList.getHumidity() == minHumidity) {
                return weatherList;

            }
        }
        return null;
    }

    @Override
    public Weather findMaxWindSpeed() {

        double maxTemp;
        List<Double> tempList = new ArrayList<>();
        for (Weather weatherList : weatherDtos) {
            tempList.add(weatherList.getWindSpeed());
        }
        maxTemp = Collections.max(tempList);
        for (Weather weatherList : weatherDtos) {
            if (weatherList.getWindSpeed() == maxTemp) {
                return weatherList;

            }
        }
        return null;
    }

    @Override
    public String findCommonWindDirection() {

        TreeMap <String, Integer> map = new TreeMap<>();

        int north = 0;
        int south = 0;
        int west = 0;
        int east = 0;

        for (Weather dto : weatherDtos) {
            if ((dto.getWindDirection() >= 315.0) || (dto.getWindDirection() <= 45.0)) north++;
            if (dto.getWindDirection() > 45.0 && dto.getWindDirection() <= 135.0) east++;
            if (dto.getWindDirection() > 135.0 && dto.getWindDirection() <= 225.0) south++;
            if (dto.getWindDirection() > 225.0 && dto.getWindDirection() < 315.0) west++;

        }

        map.put("North", north);
        map.put("East", east);
        map.put("South", south);
        map.put("West", west);

        return map.lastKey();

    }
}
